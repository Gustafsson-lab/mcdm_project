# Author: David Martínez Enguita / Jordi Serra-Musach

from script0 import *
import pandas as pd
import networkx as nx
import pylab as plt
from scipy import stats

nx.graphviz_layout = nx.nx_pydot.graphviz_layout

_dir = "/home/dme/Documents/MCDM_project/Python_scripts/data/"
X = pd.read_csv(_dir + "Validation/allcellssick_cytoscape_validation.txt", sep = "\t")

d_upreg = {}
for i in range(X.shape[0]):
    u = X.iloc[i]["from"]
    v = X.iloc[i]["to"]
    r = X.iloc[i]["what"]
    k = [u, v]
    k.sort()
    k = tuple(k)
    try:
        d_upreg[k] += [r]
    except:
        d_upreg[k] = [r]

## Number of cells
C = pd.read_csv(_dir + "Validation/CellCounts_per_sample_Joint.xls", sep = "\t", index_col = 0)
C0 = C[[v for v in C.columns if "Sick" in v]]
C = 1. * C0.sum(1) / np.sum(C0).sum()

nds = np.concatenate(list(d_upreg.keys()))
nds = np.unique(nds)
ncells = [[x for x in nds if re.search(x, v)] for v in C.index]
ncells = [ncells[i][0] if ncells[i] else C.index[i] for i in range(len(ncells))]
C.index = ncells
C = C.loc[nds]

edgs = []
for u, v in d_upreg:
    dis = d_upreg[u, v]
    w =  len(np.unique(dis))
    edgs += [(u, v, C.loc[u], C.loc[v], w)]

G = nx.Graph()
G.clear()
for u, v, n1, n2, t in edgs:
    G.add_edge(u, v, nu = n1, nv = n2, k = t, w = n1  * n2 * t)

## Random walk score
Cr =  list(nx.current_flow_betweenness_centrality(G, weight = "w").items())
Cr.sort(key = lambda v: v[1], reverse = True)
Cr = pd.DataFrame(Cr)
Cr.columns = ["cell", "prob"]
Cr.index = Cr["cell"]

ns = np.log10(Cr["prob"])
mn, mx = ns.min(), ns.max()
ns = ns - mn
ns = ns / (mx - mn)
ns = ns * (2000 - 200) + 200
ns = dict([(Cr["cell"][i], ns[i]) for i in range(len(ns))])
ns = [ns[k] for k in G.nodes()]
#pos = nx.graphviz_layout(G)

d_edgs_w = dict([((u, v), t) for u, v, a, b, t in edgs])
for u, v,_,_,_ in edgs:
    d_edgs_w[v, u] =     d_edgs_w[u, v]
edgs_w = np.array([d_edgs_w[u,v] for u, v in G.edges()])
# edgs_w = np.log(edgs_w)
mx = edgs_w.max()
mn = edgs_w.min()
edgs_w = 1. * (edgs_w - mn) / (mx - mn)
Mx = 25
Mn = 5
edgs_w = (Mx - Mn) * edgs_w + Mn
plt.figure("no-log")
#nx.draw(G, labels = dict(list(zip(*[G.nodes()] * 2))), pos = pos, node_size = ns,
        #width = edgs_w, edge_color = "#c0c0c0")
nx.draw(G, labels = dict(list(zip(*[G.nodes()] * 2))), node_size = ns,
        width = edgs_w, edge_color = "#c0c0c0")
#nx.draw_networkx_edges(G, pos = pos, width = 1, alpha = 0.25)

plt.savefig(_dir + "Validation/cell-cell_network_edge_width_5-3.pdf")

res = pd.DataFrame()
res["rw_score"] = Cr["prob"]
res.index = Cr["cell"]
res["ncell"] = 0
for v in res.index:
    u = [x for x in C.index if v in x]
    res["ncell"].loc[v] = C[u][0]

res.to_csv(_dir + "Validation/random_walk_score_RA.tsv", sep = "\t")

## Communicability Centrality
#############################

d_nds = dict([v[::-1] for v in enumerate(G.nodes())])
aux = [(v[0], v[1], v[2]["w"]) for v in G.edges(data = True)]

A = np.zeros([G.order()] * 2)
for u, v, w in aux:
    i = d_nds[u]
    j = d_nds[v]
    A[i, j] = A[j, i] = w
D = A.sum(0)

def f_norm(i, j):
    global D, A
    d = (D[i] * D[j]) ** (0.5)
    return 1. / d
    return 1. * A[i, j] / d

for i in range(A.shape[0]):
    for j in range(A.shape[1]):
        A[i,j] *= f_norm(i, j)


L, U = np.linalg.eigh(A)
U = np.array(U)

SC = (U ** 2) * np.exp(L)
SC = [SC[i,:].sum() for i in range(SC.shape[0])]

SC = pd.DataFrame(SC, index = G.nodes())
SC.columns = ["comm"]
SC.sort_values("comm", inplace = True, ascending = False)


res_com = pd.DataFrame()
res_com["SC"] = SC["comm"]
res_com.index = SC.index
res_com["ncell"] = 0
for v in res_com.index:
    u = filter(lambda x: v in x, C.index)
    res_com["ncell"].loc[v] = C[u][0]


res_com.to_csv(_dir + "Validation/subgraph_centrality_RA-healthy.csv", sep = "\t")

