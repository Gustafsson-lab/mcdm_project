# Author: Jordi Serra-Musach

from script0 import *
import pandas as pd
import networkx as nx
import pylab as plt
from scipy import stats


nx.graphviz_layout = nx.nx_pydot.graphviz_layout


_dir = Dir()
X = pd.read_excel(_dir.data + "RA_smoothed" + "allcellssick.xls", sheetname = None, index_col = None)

## Number of cells
C = pd.read_csv(_dir.data + "cell-cell_RA" +
                  "CellCounts_per_sample_Joint.xls", sep = "\t", index_col = 0)
C0 = C[filter(lambda v: "Sick" in v, C.columns)]
C = 1. * C0.sum(1) #/ np.sum(C0).sum()

## I assume that the strength of interactions is bassed in upstream
## regulators between cells that at the same time are ligand or
## receptors.

ks_up = filter(lambda k: k.startswith("up_"), X.keys())
for k in ks_up:
    z = X[k].iloc[:,0].astype("S111").isin(["nan", "", " ", "  "])
    X[k] = X[k].loc[~z,]
    X[k].columns = [X[k].columns[0].upper(), "upreg", "score"]

ks_lig = filter(lambda k: k.startswith("rece"), X.keys())
for k in ks_lig:
    z = X[k].iloc[:,0].astype("S111").isin(["nan", "", " ", "  "])
    X[k] = X[k].loc[~z,]


## Which of the pstream regulators are ligand/receptors?
clls = X.keys()
clls = filter(lambda k: k.startswith("up_"), X.keys())
clls = np.unique([v.split("_")[-1] for v in clls])
cllx = X.keys()
cllx = filter(lambda k: k.startswith("recep"), X.keys())
cllx = np.unique([v.split("_")[-1] for v in cllx])
print all(cllx == clls)


edgs = []
upreg = {}
for cll in clls:
    aux = X["up_regulators_%s"%cll][[cll.upper(), "upreg"]].groupby(cll.upper())["upreg"].apply(list)
    x = X["receptors_ligands_%s"%cll].iloc[:,0]
    w1 = filter(lambda x: cll in x, C.index)
    for cll2 in aux.index:
        y = filter(lambda x: not isinstance(x, float), aux.loc[cll2])
        # y = aux.loc[cll2]
        w2 = filter(lambda x: cll2 in x, C.index)
        edgs += [(cll2, cll, C[w2][0], C[w1][0], len(y))]
        if not (cll2, cll) in upreg:
            upreg[cll2, cll] = []
        upreg[cll2, cll] = tuple(np.union1d(y, upreg[cll2, cll]))

nds = np.unique(np.concatenate([v[:2] for v in edgs]))

new_edgs = []
for i in range(len(nds) - 1):
    u = nds[i]
    for j in range(i + 1, len(nds)):
        v = nds[j]
        aux = [w for w in edgs if (w[0] == u and w[1] == v ) or (w[0] == v and w[1] == u)]
        a = [w[-1] for w in aux]
        new_edgs += [(u, v, aux[0][2], aux[0][3], sum(a))]

        
G = nx.Graph()
G.clear()
for u, v, n1, n2, t in new_edgs:
    G.add_edge(u, v, nu = n1, nv = n2, k = t, w = n1  * n2 * t, w2 = 1. / n1 / n2 / t, w3 = np.pi - n1  * n2 * t)


## Random walk score
Cr =  nx.current_flow_betweenness_centrality(G, weight = "w").items()
Cr.sort(key = lambda v: v[1], reverse = True)
Cr = pd.DataFrame(Cr)
Cr.columns = ["cell", "prob"]

ns = np.log10(Cr["prob"])
mn, mx = ns.min(), ns.max()
ns = ns - mn
ns = ns / (mx - mn)
ns = ns * (2000 - 200) + 200
ns = dict([(Cr["cell"][i], ns[i]) for i in range(len(ns))])
ns = [ns[k] for k in G.nodes()]
pos = nx.graphviz_layout(G)

d_edgs_w = dict([((u, v), t) for u, v, a, b, t in new_edgs])
for u, v,_,_,_ in new_edgs:
    d_edgs_w[v, u] =     d_edgs_w[u, v]
edgs_w = np.array([d_edgs_w[u,v] for u, v in G.edges()])
# edgs_w = np.log(edgs_w)
mx = edgs_w.max()
mn = edgs_w.min()
edgs_w = 1. * (edgs_w - mn) / (mx - mn)
Mx = 25
Mn = 5
edgs_w = (Mx - Mn) * edgs_w + Mn
plt.figure("no-log")
nx.draw(G, labels = dict(zip(*[G.nodes()] * 2)), pos = pos, node_size = ns,
        width = edgs_w, edge_color = "#c0c0c0")
nx.draw_networkx_edges(G, pos = pos, width = 1, alpha = 0.25)

plt.savefig(_dir.images + "cell-cell_network_edge_width.pdf")


plt.plot(Cr["prob"], "bo-")
plt.ylabel("betweenness centrality coefficient")
plt.xticks(range(Cr.shape[0]), Cr["cell"], rotation = 45)
plt.savefig(_dir.images + "RA_centrality.pdf")

res = pd.DataFrame()
res["rw_score"] = Cr["prob"]
res.index = Cr["cell"]
res["ncell"] = 0
for v in res.index:
    u = filter(lambda x: v in x, C.index)
    res["ncell"].loc[v] = C[u][0]

res.to_csv(_dir.results + "random_walk_score_RA.tsv", sep = "\t")
    
## GWAS enrichment
## correlation between centrality and GWAS enrichment:
## spearman pval = 0.04
GW = pd.read_csv(_dir.data + "cell-cell_RA" +
                 "gwas_cell_vs_all_others.csv", sep = "\t", index_col = 0)
rep = np.intersect1d(res.index, GW.index)
x = res.loc[rep]
GW = GW.loc[rep,]

plt.figure("log")
plt.plot(x["rw_score"], GW["overlap"], "o")
print stats.pearsonr(x["rw_score"], GW["overlap"])
print stats.spearmanr(x["rw_score"], GW["overlap"])
print stats.kendalltau(x["rw_score"], GW["overlap"])

a, b, r_value, p_value, std_err = stats.linregress(np.log10(x.rw_score), GW["overlap"])
X = np.log10(np.logspace(-3, 0, 100)) #np.log10(x.rw_score)
Y = a * X + b
ixs = np.argsort(X)[::-1]
plt.plot(10 ** ((Y[ixs] - b) / a), Y[ixs], "r-", lw = 2)


## logaritmic
plt.figure("linear")
plt.plot(np.log10(x["rw_score"]), GW["overlap"], "o")
a, b, r_value, p_value, std_err = stats.linregress(np.log10(x.rw_score), GW["overlap"])
plt.plot(np.log10(x["rw_score"]), a * np.log10(x["rw_score"]) + b, "r-", lw = 2)
print stats.pearsonr(np.log10(x["rw_score"]), GW["overlap"])
print stats.spearmanr(x["rw_score"], GW["overlap"])
print stats.kendalltau(x["rw_score"], GW["overlap"])



plt.figure()
plt.plot(x["rw_score"], GW["OR"], "o")
print stats.pearsonr(x["rw_score"], GW["OR"])
print stats.spearmanr(x["rw_score"], GW["OR"])
print stats.kendalltau(x["rw_score"], GW["OR"])

## expressed genes
GW = pd.read_csv(_dir.data + "cell-cell_RA" +
                 "gwas_expressed_genes.csv", sep = "\t", index_col = 0)
rep = np.intersect1d(res.index, GW.index)
x = res.loc[rep]
GW = GW.loc[rep,]

plt.figure()
plt.plot(x["rw_score"], GW["overlap"], "o")
print stats.pearsonr(x["rw_score"], GW["overlap"])
print stats.spearmanr(x["rw_score"], GW["overlap"])
print stats.kendalltau(x["rw_score"], GW["overlap"])

plt.figure()
plt.plot(x["rw_score"], GW["OR"], "o")
print stats.pearsonr(x["rw_score"], GW["OR"])
print stats.spearmanr(x["rw_score"], GW["OR"])
print stats.kendalltau(x["rw_score"], GW["OR"])




## Lymph node
#############
## number of genes in T and B cells?
C = pd.read_csv(_dir.data + "cell-cell_RA" +
                  "CellCounts_per_sample_LymphNode.xls", sep = "\t", index_col = 0)
C = C[filter(lambda v: "Sick" in v, C.columns)]
C = C.sum(1)

## tcells: 253
## bcells: 244
print stats.binom_test([C["T-cells-CD4-"], 260])



## Communicability Centrality
#############################

d_nds = dict([v[::-1] for v in enumerate(G.nodes())])
aux = [(v[0], v[1], v[2]["w"]) for v in G.edges(data = True)]

A = np.zeros([G.order()] * 2)
for u, v, w in aux:
    i = d_nds[u]
    j = d_nds[v]
    A[i, j] = A[j, i] = w
D = A.sum(0)

def f_norm(i, j):
    global D, A
    d = (D[i] * D[j]) ** (0.5)
    return 1. / d
    return 1. * A[i, j] / d

for i in range(A.shape[0]):
    for j in range(A.shape[1]):
        A[i,j] *= f_norm(i, j)


L, U = np.linalg.eigh(A)
U = np.array(U)

SC = (U ** 2) * np.exp(L)
SC = [SC[i,:].sum() for i in range(SC.shape[0])]

SC = pd.DataFrame(SC, index = G.nodes())
SC.columns = ["comm"]
SC.sort_values("comm", inplace = True, ascending = False)


res_com = pd.DataFrame()
res_com["SC"] = SC["comm"]
res_com.index = SC.index
res_com["ncell"] = 0
for v in res_com.index:
    u = filter(lambda x: v in x, C.index)
    res_com["ncell"].loc[v] = C[u][0]


res_com.to_csv(_dir.results + "subgraph_centrality_RA.csv", sep = "\t")


    
## GWAS enrichment
## correlation between centrality and GWAS enrichment:
## spearman pval = 0.04
GW = pd.read_csv(_dir.data + "cell-cell_RA" +
                 "gwas_cell_vs_all_others.csv", sep = "\t", index_col = 0)
rep = np.intersect1d(res_com.index, GW.index)
x = res_com.loc[rep]
GW = GW.loc[rep,]

plt.figure("log")
plt.plot(x["SC"], GW["overlap"], "o")

print "Corr Subgraph centrality vs GWAS"
print stats.pearsonr(x["SC"], GW["overlap"])
print stats.spearmanr(x["SC"], GW["overlap"])
print stats.kendalltau(x["SC"], GW["overlap"])

a, b, r_value, p_value, std_err = stats.linregress(np.log10(x.SC), GW["overlap"])
X = np.log10(np.logspace(-3, 0.5, 100)) #np.log10(x.rw_score)
Y = a * X + b
ixs = np.argsort(X)[::-1]
plt.plot(10 ** ((Y[ixs] - b) / a), Y[ixs], "r-", lw = 2)


plt.figure("lin")
plt.plot(np.log10(x["SC"]), GW["overlap"], "o")

print "Corr Log(Subgraph centrality) vs GWAS"
print stats.pearsonr(np.log10(x["SC"]), GW["overlap"])
print stats.spearmanr(np.log10(x["SC"]), GW["overlap"])
print stats.kendalltau(np.log10(x["SC"]), GW["overlap"])

a, b, r_value, p_value, std_err = stats.linregress(np.log10(x.SC), GW["overlap"])
X = np.log10(np.logspace(-3, 0.5, 100)) #np.log10(x.rw_score)
Y = a * X + b
ixs = np.argsort(X)[::-1]
plt.plot(np.log10(x.SC), a * np.log10(x.SC) + b, "r-", lw = 2)
