# Author: Jordi Serra-Musach

from script0 import *
import networkx as nx
import pylab as plt

_dir = Dir()


G = nx.read_adjlist(_dir.HOME + "workbench/string" +
                    "20180508_string_10090/data" +
                    "graph_string_v-2018_taxid-10090" + "Mgi" +
                    "graph_string-v-2018_taxid-10090_all-interactions_sorted-numerically-by-mgiid.tsv",
                    nodetype = int)

## some metrics of the network
print G.order(), G.size()
degs = np.array(G.degree().values())
plt.cla()
h = plt.hist(degs, bins = 150)
plt.loglog()

A = pd.read_csv(_dir.HOME + "workbench/string" +
                "20180508_string_10090/data" +
                "graph_string_v-2018_taxid-10090" + "Mgi" +
                "graph_string-v-2018_gene_ids_conversion_taxid-10090_all-interactions_sorted-numerically-by-mgiid.tsv",
                sep = "\t")

## Data Frame with columns
##    ensmble-protein_id   node_id   mgi_id
Ids = zip(A["protein1"], A["node1"],
          [int(v) if not np.isnan(v) else "" for v in A["MgiID1"]],
          [str(int(v)) if not np.isnan(v) else "" for v in A["EntrezGeneID1"]])
Ids += zip(A["protein2"], A["node2"],
           [int(v) if not np.isnan(v) else "" for v in A["MgiID2"]],
           [str(int(v)) if not np.isnan(v) else "" for v in A["EntrezGeneID2"]])

Ids = pd.DataFrame(Ids)
Ids.columns = ["EnsmblProtID", "NodeID", "MgiID", "EntrezGeneID"]
Ids.drop_duplicates(inplace = True)
## grouping by node-ids
Ids = Ids.groupby("NodeID")
Ids = pd.concat((Ids["MgiID"].unique().apply(lambda v: ";".join(v.astype("S111"))),
                 Ids["EntrezGeneID"].unique().apply(";".join),
                 Ids["EnsmblProtID"].unique().apply(";".join)), axis = 1)
Ids["vertex"] = Ids.index
Ids["neighbours_vertex"] = [set(nx.neighbors(G, v)) for v in Ids["vertex"]]
Ids["no_neighbours"] = [nx.degree(G, v) for v in Ids["vertex"]]
Ids = Ids[["vertex", "neighbours_vertex", "no_neighbours", "MgiID", "EntrezGeneID", "EnsmblProtID"]]


Ids["neighbours_vertex"] = [";".join(np.array(list(Ids["neighbours_vertex"].iloc[i])).astype("S111"))  for i in range(Ids.shape[0])]

Ids.to_csv(_dir.data + "mouse-ppin_by-mgiid.dat", sep = "\t", index = False)
