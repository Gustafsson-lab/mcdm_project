# Author: Jordi Serra-Musach

import numpy as np
import re
import os
import socket
import time
import pandas as pd

class Str(str):
    def __add__(self,x):
        aux = self.endswith('/') + x.startswith('/')
        if aux==0:
            return Str(super(Str, self).__add__('/'+x))
        if aux == 1:
            return Str(super(Str, self).__add__(x))
        x = str(x).strip('/')
        return Str(super(Str, self).__add__(str(x)))
    def format(self, value):
        x = super(Str, self).format(value)
        return Str(x)

class Dir(object):
    host = socket.gethostname()
    _data = 'data'
    _docs = 'docs'
    _images = 'images'
    _results = 'results'
    _scripts = 'scripts'
    _interactome = 'data/Interactome'
    _dropbox = 'Dropbox'
    _kegg = 'data/KEGGs/2014_4_30'
    _Data = "Data" # data in $HOME folder
    _tmp = "tmp"
    _TMP = "tmp"
    _wf = "warefolder"
    def __init__(self):
        """
        Contains all paths to be used
        """
        if self.host == 'dori':
            self.HOME = Str('/illumina/runs2/Jordi')
        else:
            self.HOME = Str(os.path.expanduser('~'))
            self.set_home(os.getcwd().replace('scripts',''))
    def set_home(self, path):
        if path.endswith('/'):
            path = path[:-1]
        self._home = Str(path)
    def get_home(self):
        return self._home
    def del_home(self):
        del self._home
    home = property(get_home, set_home, del_home)

    def decorator(attr1, attr2):
        def wrap(func):
            def __set(self):
                home = self.__getattribute__(attr1)
                x = self.__getattribute__(attr2)
                x = Str(home) + Str(x)
                return x
            return __set
        return wrap

    @decorator('home', '_data')
    def _get(self):
        pass
    data = property(_get)

    @decorator('home', '_docs')
    def _get(self):
        pass
    docs = property(_get)

    @decorator('home', '_results')
    def _get(self):
        pass
    results = property(_get)

    @decorator('home', '_scripts')
    def _get(self):
        pass
    scripts = property(_get)

    @decorator('home', '_tmp')
    def _get(self):
        pass
    tmp = property(_get)

    @decorator('HOME', '_interactome')
    def _get(self):
        pass
    interactome = property(_get)

    @decorator('HOME', '_TMP')
    def _get(self):
        pass
    TMP = property(_get)

    @decorator('home', '_images')
    def _get(self):
        pass
    images = property(_get)

    @decorator('HOME', '_dropbox')
    def _get(self):
        pass
    dropbox = property(_get)

    @decorator('dropbox', '_kegg')
    def _get(self):
        pass
    kegg = property(_get)
    
    def ls(self, dr=None):
        if dr is None:
            dr = self.home
        os.system("ls -lh %s"%dr)

    @decorator('HOME', '_Data')
    def _get(self):
        pass
    Data = property(_get)

    @decorator('HOME', '_wf')
    def _get(self):
        pass
    wf = property(_get)

        

def Carrega_fitxer(path, ext = None, line_sep = None, col_sep = None,
                   rm_quote=None, verbatim=True):
    if ext:
        path += '.' + ext
    else:
        if path.count('.') == 0:
            ext = ''
        else:
            ext = path.split('.')[-2:]
            if ext[0] == 'pickle' and ext[1] == 'gz':
                ext = '.'.join(ext)
            else:
                ext = ext[-1]
    try:
        fit=file(path)
    except:
        pass
        # if ext and path.count('.') > 1:
        #     l = len(ext)
        #     path[-2*l-1:-l-1] == ext
        #     raise ValueError("May path value be '%s' rather than '%s'?"%(path[:-l],path))
        # elif not ext:
        #     raise ValueError("May you forget the extension file?")
    if ext == 'pickle':
        import cPickle as pickle
        fit = open(path)
        aux = pickle.load(fit)
        fit.close()
    elif ext == 'gz':
        import gzip
        f = gzip.open(path, 'rb')
        aux = f.read()
        f.close()
    elif ext == 'pickle.gz':
        import gzip
        import cPickle as pickle
        f = gzip.open(path, 'rb')
        aux = f.read()
        f.close()
        aux = pickle.loads(aux)
    else:
        fit = open(path)
        aux = fit.read()
        fit.close()
    if not rm_quote is None:
        aux = aux.replace('"','')
    if line_sep == None:
        return aux
    aux = aux.strip(line_sep)
    aux = aux.split(line_sep)
    if col_sep == None:
        return aux
    aux = [v.split(col_sep) for v in aux]
    if verbatim:
        print ("\n The file\n")
        print (path)
        print ("\nhave been loaded successfully\n")   
    return aux


def Escriu(x, path, ext = None, level = None, mode='w', silence = False):
    """
    Escriu l'element x al fitxer path amb format format.
    Si el format es pickle, nivell es el nivell de compressio.
    El mode correspon al mode de la funcio file ('w','a',...)
    """
    if not silence:
        s = time.time()
        print ("Guardant...")
    if not ext is None:
        path += '.' + ext
    else:
        if path.count('.') == 0:
            ext = 'pickle'
            path = path + '.' + ext
        else:
            ext = path.split('.')[-2:]
            if ext[0] == 'pickle' and ext[1] == 'gz':
                ext = '.'.join(ext)
            else:
                ext = ext[-1]
    if ext == 'pickle':
        import cPickle as pickle
        level = 2 if level == None else level
        fit = open(path, mode)
        pickle.dump(x, fit, level)
        fit.close()
        if not silence:
            t = time.time()
            #print "Guardat a (%.2f seconds) '%s' at compress level %d"%(t-s, path, level)
    elif ext == 'gz':
        import gzip
        level = 9 if level == None else level
        FIT = gzip.open(path, mode = mode, compresslevel=level)
        FIT.write(x)
        FIT.close()
        if not silence:
            t = time.time()
            #print "Guardat a (%.2f seconds) '%s' at compress level %d"%(t-s, path, level)
    elif ext == 'pickle.gz':
        import cPickle as pickle
        import gzip
        level = 9 if level == None else level
        FIT = gzip.open(path, mode = mode, compresslevel=level)
        x = pickle.dumps(x, 2)
        FIT.write(x)
        FIT.close()
        if not silence:
            t = time.time()
            #print "Guardat a (%.2f seconds) '%s' at compress level %d"%(t-s, path, level)
    else:
        fit = open(path, mode)
        try:
            fit.write(x)
            if not silence:
                t = time.time()
                #print "Guardat a (%.2f seconds) '%s'"%(t-s, path)
        except:
            raise TypeError("You may check the type object to save 'x'")
        fit.close()



class GeneEntrez(object):
    _dir = Dir()
    def __init__(self):
        self.x = self._dir.wf + "data" + "NCBI" + "gene_info_taxid-9606_version-20180508.tsv.gz"
        self.x = pd.read_csv(self.x, delimiter = "\t")
    def f_id2sym(self, gene, key = True, sym_source = "HGNC"):
        """
        Arguments
        ---------
        key is True, return a dictionary with gene Ids as keys
        sym_source: HGNC|NCBI. use columns 10 or 2 respectively
        """
        if sym_source == "HGNC":
            sym = 'Symbol_from_nomenclature_authority'
        elif sym == "NCBI":
            sym = "Symbol"
        if hasattr(gene, "__iter__"):
            gene = np.array(gene, dtype = int)
            gene = np.unique(gene)
        else:
            gene = np.array([gene], dtype = int)
        y = self.x.loc[self.x["GeneID"].isin(gene), ["GeneID", sym]]
        if key  is True:
            y.drop_duplicates(inplace = True)
            y = [(y.iloc[i]["GeneID"], y.iloc[i][sym]) for i in range(y.shape[0])]
            n = len(y)
            y = dict(y)
            m = len(y)
            if m == n:
                return y
            raise Error("No unique items!!!")
        return y[sym].unique()
    
    def f_sym2id(self, gene, key = True, sym_source = "HGNC"):
        """
        if key is True, return a dictionary with Symbols as keys
        """
        if sym_source == "HGNC":
            sym = 'Symbol_from_nomenclature_authority'
        elif sym == "NCBI":
            sym = "Symbol"
        if hasattr(gene, "__iter__"):
            gene = np.unique(gene)
        else:
            gene = np.array([gene])
        y = self.x.loc[self.x[sym].isin(gene), [sym, "GeneID"]]
        if key  is True:
            y.drop_duplicates(inplace = True)
            ## are the keys unique?
            if len(y[sym]) == len(y[sym].unique()):
                return dict(zip(y[sym], y["GeneID"]))
            else:
                D = {}
                for i in range(y.shape[0]):
                    try:
                        D[y.iloc[i][sym]] += [y.iloc[i]["GeneID"]]
                    except:
                        D[y.iloc[i][sym]] = [y.iloc[i]["GeneID"]]
                return D
        return y

    def getColumns(self):
        return self.x.columns
    columns = property(getColumns)
    def head(self, *args):
        return self.x.head(*args)
    def __getitem__(self, keys):
        return self.x[keys]
        


        
