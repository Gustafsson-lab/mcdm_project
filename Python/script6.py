# Author: Jordi Serra-Musach

from script0 import *
import networkx as nx
import pylab as plt
import seaborn as sns


nx.graphviz_layout = nx.nx_pydot.graphviz_layout


_dir = Dir()

X =  pd.read_csv(_dir.data + "CD_UC_goterms.tsv", sep = "\t")

# p = re.compile(",G")
# ixs = p.finditer(X["GOTERM_BP_DIRECT"][0])
# ixs = [0] + [v.start() for v in ixs] + [len(X["GOTERM_BP_DIRECT"][0])]

aux = X["GOTERM_BP_DIRECT"].apply(lambda v: [w.split("~")[0] for w in re.sub(",G", ";G", v).split(";")])


aux = [(X["ID"][i], v) for i in range(X.shape[0]) for v in aux[i]]
aux = pd.DataFrame(aux)
aux.columns = ["EntrezGeneID", "GOID"]

edgs = pd.merge(aux, aux, left_on = "GOID", right_on = "GOID")
edgs = edgs.groupby(["EntrezGeneID_x", "EntrezGeneID_y"])["GOID"].apply(len)
edgs = [(edgs.index[i][0], edgs.index[i][1], edgs.iloc[i]) for i in range(edgs.shape[0])]
edgs = filter(lambda u: u[0] != u[1], edgs)

nds = np.unique(X["ID"])
edgs = filter(lambda u: u[2] > 0, edgs)
edgs = [(list(v)[:2], v[-1]) for v in edgs]
[v[0].sort() for v in edgs]
edgs = [(v[0], v[1], u) for v, u in edgs]
edgs = pd.DataFrame(edgs)
edgs.drop_duplicates(inplace = True)
edgs = [(edgs[0].iloc[i], edgs[1].iloc[i], edgs[2].iloc[i]) for i in range(edgs.shape[0])]

G = nx.Graph()
G.add_nodes_from(nds)
for u, v, w in edgs:
    G.add_edge(u, v, w = w)
print "\n", G.order(), G.size()

pos = nx.graphviz_layout(G)


plt.cla()
nx.draw(G, pos = pos)

d_nds = dict([(v, i) for i, v in enumerate(nds)])

A = np.zeros([len(nds)] * 2)
for u, v, w in edgs:
    A[d_nds[u], d_nds[v]] = A[d_nds[v], d_nds[u]] = w

d_edgs = dict([((u, v), w) for u, v,  w in edgs])
    
def f_weight(G):
    d_edgs = {(u,v) : w["w"] for u, v, w in G.edges(data = True)}
    return max(d_edgs, key = d_edgs.get)

com = nx.community.girvan_newman(G, most_valuable_edge = f_weight)
coms = list(com)

for v in coms:
    print len(v)

x = np.array(map(lambda v: nx.algorithms.community.modularity(G, v, weight = "w"), coms))
y = np.array(map(lambda v: nx.algorithms.community.modularity(G, v), coms))
print np.argmax(x), np.argmax(y)

Com = coms[34]

for v in Com:
    print len(v)



sns.clustermap(A, center=0, cmap="bwr", metric = "euclidean",
               linewidths=.75, figsize=(13, 13))
