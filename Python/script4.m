# Author: Jordi Serra-Musach

function script4(tissue)
% % %% path to save data:
%% PATHH = '/proj/sharedmech/users/x_danga/drugs_shared/results/';
%PATHH='C:\Users\danga10\Documents\SHARED\data\cluster_results\drugs\locally\'
% clear all
% close all
% clc
% tissue='joint';


PATHH = '/home/jordi/Projects/Shared/Shared/data/matlab/'

addpath('/home/jordi/Shared/Shared')
load('drug_predictions/drugs_data_cluster2')

%% loading PPIn
G = readtable('/home/jordi/Projects/Shared/Shared/data/mouse-ppin_by-mgiid.dat');
[n,~] = size(G);
aux = {};
auy = {};
for i=1:n
    v = char(table2array(G(i,'neighbours_vertex')));
    v = strsplit(v, ';');
    v = str2double(v);
    w = char(table2array(G(i,'EntrezGeneID')));
    w = strsplit(w, ';');
    aux{i} = v;
    auy{i} = w;
end
G(:,'neighbours_vertex') = aux';
G(:,'EntrezGeneID') = auy';
[n, ~] = size(G);
aux = {};
for i = 1 : n
    aux{i} = num2str(G.MgiID(i));
end
G.MgiID = aux';

%% creating bins
N = 1000; % number if genes in each bin
udegs = unique(G.no_neighbours);
s = 1;
ks = [];
[nG, ~] = size(G);
b = 0;

while s > 0
    s2 = 1;
    k = udegs(s);
    z = false([nG, 1]);
    disp([s, length(udegs), k])
    while s2 > 0
        z2 = G.no_neighbours == k;
        znew = z | z2;
        if sum(znew) < N
            kold = k;
            z = znew;
            if (s + s2) <= length(udegs)
                k = udegs(s + s2);
                s2 = s2 + 1;
            else
                s = -1;
                s2 = -1;
            end
        else
            if s2 == 1
                z = znew;
                kold = k;
                s = s + s2;
            else
                m = 1000 - sum(z);
                n = sum(znew) - 1000;
                if n < m
                    z = znew;
                    kold = k;
                    s = s + s2;
                else
                    s = s + s2 - 1;
                end
            end
            s2 = -1;
        end
        clear znew;
        clear z2;
    end
    G.bin(z) = b;
    b = b + 1;
    ks = [ks, kold];
    if s > length(udegs)
        s = -1;
    end
end


%% For Perm
ForPerm_nds = {};
for i = 0 : max(G.bin)
    ForPerm_nds{i+1} = table2array(G(G.bin == i, {'vertex'}));
end


%% Drug Targets info
D = readtable(['/home/jordi/Projects/Shared/Shared/data/' ...
               'drug_to_ortholog_targets.dat'], 'Delimiter', '\t');

[n, ~] = size(D);
for i = 1 : n
    v = strsplit(D.MGI_ID_mouse{i}, ':');
    D.MGI_ID_mouse{i} = v{2};
end

%% removing drugs with no targets in the PPIn
z = ismember(D.MGI_ID_mouse, G.MgiID);
D = D(z,:);
% drugs = dataset2table(drugs);
% writetable(drugs, '/home/jordi/Projects/Shared/Shared/data/drugs_info.csv')


%% Distance matrix of PPIn
Dist = readtable(['/home/jordi/workbench/string/' ...
                  '20180508_string_10090/data/graph_string_v-' ...
                  '2018_taxid-10090/Mgi/graph_string-v-2018_dist-' ...
                  'matrix_taxid-10090_all-interactions_sorted-' ...
                  'numerically-by-mgiid.dat'], 'Delimiter', ' ');
Dist = table2array(Dist);

%% Disease Genes
%% Diseases Genes are in EntrezGeneID
%% So must be translated to mgi IDs
if tissue == 'joint'
    disp(['fucking ', tissue])
    DG = readtable(['/home/jordi/Projects/Shared/Shared/data/' ...
                    'RA_smoothed/DEGS_with_Monocle_Joint/' ...
                    'Monocle_out/Monocle_DEGs_Joint_adipose-' ...
                    'white_RAvsHealthy.dat'], 'Delimiter', ' ');
    z = DG.pval < 0.05;
    DG = DG(z,:);
    G.MgiID(cellfun(@(c) ismember('66839', c), G.EntrezGeneID))

    % aux = cellfun(@(v) G.MgiID(cellfun(@(c) ismember(v, c), ...
    %                                    G.EntrezGeneID)), DG.gene_short_name, 'UniformOutput', false);
    % DG.MgiID = aux;
    %% '102577427'  '102577428'
    [n , ~] = size(DG);
    aux = {};
    for i = 1 : n
        v = DG.gene_short_name{i};
        z = cellfun(@(c) ismember(v, c), G.EntrezGeneID);
        aux{i} = num2str(G.MgiID(z));
    end

    for i = 1 : n
        if isempty(aux{i})
            aux{i} = ''
        else
            aux{i} = num2str(aux{i})
        end
    end
    DG.MgiID = aux';
    z = cellfun(@(c) isempty(c), DG.MgiID);
    DG = DG(~z,:);

    
% load('drugs_data_cluster_Pivmecillinam')

%load drugs_data_cluster2

nperm = 10;
udrugs = unique(drugs.Drug_IDs);
udrugs = unique(D.Drug_IDs);

%% for parallelasing
 try
     parpool_obj = parpool('local',16);
 catch
     pause(rand*600);
     parpool_obj = parpool('local',16);
 end
disp(diz)
% prop_dd_out_to_inside =
% sum(~ismember(disease_genes.genes{diz},module))/sum(ismember(disease_genes.genes{diz},module));
% % same for all drugs

%% Playing with MATLAB objects %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% print all loaded objects
whos

length(fieldnames(disease_genes))
fieldnames(disease_genes)
structfun(@(c) class(c), disease_genes, 'UniformOutput', false)
structfun(@(c) size(c), disease_genes, 'UniformOutput', false)

nfields = fieldnames(disease_genes)
for i = 1:length(nfields)
    size(disease_genes.(nfields{i}))
end
disease_genes.genes
disease_genes.dis

class(drugs)
get(drugs)
get(drugs, 'DimNames')
get(drugs, 'VarNames')

net2.Properties
net2.Properties.VariableNames
net2(1:11,1:end)
net2(1, {'neighbours_vertex'})

for i=1:length(ForPerm_idx)
    z = ismember(net2.vertex, ForPerm_idx{i});
    kv = net2(z, 'no_neighbours');
    disp([i, mean(table2array(kv)), sum(z)])
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% Playing with MATLAB objects %%

udrugs = unique(D.Drug_IDs);

for z = 1 : length(udrugs)
    if mod(z, 100) == 0
        disp(join([z, "/", length(udrugs)]))
    end
    T = unique(D.MGI_ID_mouse(strcmp(D.Drug_IDs,udrugs{z}))); % drug targets
    ixs_t = ismember(G.MgiID, T);
    ixs_t = G.vertex(ixs_t);
    
    % drug target distances to disease genes:
    ixs_dd = ismember(G.MgiID, DG.MgiID); %row positions of disease
                                          %genes
    ixs_dd = G.vertex(ixs_dd);
    ixs_dd = ixs_dd + 1;
    ixs_t = ixs_t + 1;
    H = Dist(ixs_dd, ixs_t); % in rows disease genes in columns drug targets
    ODL(z, 1) = mean(min(H)); % distance target disease gene
    clear  T;
end

%% save (sprintf('%spivmecillinam_drugs_%s',PATHH,disease_genes.dis{diz}))

% I will prioritize for calculations those that are close with D1
% measurement col7
[~,idx] = sort(ODL(:,7),'ascend');
udrugs = udrugs(idx);
ODL = ODL(idx,:);

nethelp = net2.EntrezGeneID;
distancehelp = distance2;
net22 =net2;
module2 = module;
pomocnicza=0;




for z = 1 : length(udrugs)
    if mod(z, 100) == 0
        disp(join([z, "/", length(udrugs)]))
    end
    pomocnicza=pomocnicza+1;
    T = unique(D.MGI_ID_mouse(strcmp(D.Drug_IDs,udrugs{z}))); % drug targets
    ixs_t = ismember(G.MgiID, T);
    ixs_t = G.vertex(ixs_t);
    % random:
    target_degree = G(ismember(G.MgiID,T),{'bin'});
    target_degree = varfun(@mean,target_degree,'Groupingvariables','bin');
    diseaseG_degree = G(ismember(G.MgiID, DG.MgiID),{'bin'});
    diseaseG_degree = varfun(@mean,diseaseG_degree,'Groupingvariables','bin');
    ODL_perm = NaN(nperm, 1);
    for (perm = 1 : nperm)%,16)
        %% taking the same number of genes as target genes with in
        %% the same bin
        rixs_t = randsample(ForPerm_nds{1 + target_degree.bin(1)}, target_degree.GroupCount(1));
        rixs_d = randsample(ForPerm_nds{1 + diseaseG_degree.bin(1)}, ...
                            diseaseG_degree.GroupCount(1));
        %% Do the same in case many bins
        for t = 2 : length(target_degree.bin)
            rixs_t = [rixs_t; randsample(ForPerm_idx{1 + ...
                                target_degree.bin(t)}, target_degree.GroupCount(t))];
        end
        for t = 2 : length(diseaseG_degree.bin)
            rixs_d = [rixs_d; randsample(ForPerm_nds{1 + ...
                                diseaseG_degree.bin(t)}, diseaseG_degree.GroupCount(t))];
        end
        rixs_t = rixs_t + 1;
        rixs_d = rixs_d + 1;
        rH = Dist(rixs_d, rixs_t); % in rows disease genes in columns drug targets
        ODL_perm(perm, 1) = mean(min(rH)); % distance target
                                           % disease gene
        clear rixs_d rixs_t
    end
    % want to d_t_dd_out be big and the rest to be small:
    zscore(z,1) = (ODL(z,1) - mean(ODL_perm)) ./ std(ODL_perm);
    p_val_perm(z,1) = mean(repmat(ODL(z,1), nperm,1) > ODL_perm);
    [~, p_zscore(z, 1)] = ztest(zscore(z,1),0,1,[],'left');
    % inverse tests for d_t_dd_out at pos 4:
    if pomocnicza==100 || z==length(udrugs)
        save(sprintf('%spivmecillinam_drugs_drug%s_%s', PATHH, ...
                      num2str(z), tissue))
        pomocnicza=0;
    end
    clear T target_degree diseaseG_degree diseaseG_degree_in diseaseG_degree_out
end

res.col_description = dataset({{'d_t_dd'}','abreviation'})
                            
res.zscore = [dataset({udrugs(1:size(zscore,1)),'drugID'}), ...
              mat2dataset(zscore,'varnames', ...
                          res.col_description.abreviation)];   

res.p_val_perm = [dataset({udrugs(1:size(p_val_perm,1)), ...
                    'drugID'}),mat2dataset(p_val_perm,'varnames', ...
                                           res.col_description ...
                                           .abreviation)];   


res.p_zscore = [dataset({udrugs(1:size(p_zscore,1)),'drugID'}), ...
                    mat2dataset(p_zscore,'varnames', ...
                                res.col_description.abreviation)];   

save (sprintf('%spivmecillinam_drugs_%s_end', PATHH,tissue))
delete(parpool_obj)
