# Author: David Martínez Enguita

import pandas as pd
import networkx as nx
import pylab as plt
import numpy as np
from scipy import stats
import random
import string
import warnings
import sys

# Result objects and parameters
result_rw = []
result_sc = []
perturb_value = 0.1

# Input data paths
allcellssick_xls = "/home/dme/Documents/MCDM_project/Python_scripts/data/RA_smoothed/allcellssick.xls"
cellcountspersample_xls = "/home/dme/Documents/MCDM_project/Python_scripts/data/cell-cell_RA/CellCounts_per_sample_Joint.xls"

# Function for random unique tag creation
def id_generator(size = 6, chars = string.ascii_uppercase + string.digits):
    return ''.join(random.choice(chars) for _ in range(size))

for q in range(0, 100000):
    try:
        ## Random Walk (Unperturbed)
        X = pd.read_excel(allcellssick_xls, sheetname = None, index_col = None)
        ks_up = [k for k in list(X.keys()) if k.startswith("up_")]
        for k in ks_up:
            z = X[k].iloc[:,0].astype("S111").isin(["nan", "", " ", "  "])
            X[k] = X[k].loc[~z,]
            X[k].columns = [X[k].columns[0].upper(), "upreg", "score"]

        ks_lig = [k for k in list(X.keys()) if k.startswith("rece")]
        for k in ks_lig:
            z = X[k].iloc[:,0].astype("S111").isin(["nan", "", " ", "  "])
            X[k] = X[k].loc[~z,]

        C = pd.read_csv(cellcountspersample_xls, sep = "\t", index_col = 0)
        C0 = C[[v for v in C.columns if "Sick" in v]]
        C = 1. * C0.sum(1) / np.sum(C0).sum()
        clls = list(X.keys())
        clls = [k for k in list(X.keys()) if k.startswith("up_")]
        clls = np.unique([v.split("_")[-1] for v in clls])
        cllx = list(X.keys())
        cllx = [k for k in list(X.keys()) if k.startswith("recep")]
        cllx = np.unique([v.split("_")[-1] for v in cllx])
        
        
        edgs = []
        upreg = {}
        for cll in clls:
            aux = X["up_regulators_%s"%cll][[cll.upper(), "upreg"]].groupby(cll.upper())["upreg"].apply(list)
            if (cll == 'treg'): aux.index = ['adipo', 'gran', 'macro', 'myelo'] 
            x = X["receptors_ligands_%s"%cll].iloc[:,0]
            w1 = [x for x in C.index if cll in x]
            for cll2 in aux.index:
                y = [x for x in aux.loc[cll2] if not isinstance(x, float)]
                w2 = [x for x in C.index if cll2 in x]
                edgs += [(cll2, cll, C[w2][0], C[w1][0], len(y))]
                if not (cll2, cll) in upreg:
                    upreg[cll2, cll] = []
                upreg[cll2, cll] = tuple(np.union1d(y, upreg[cll2, cll]))

        nds = np.unique(np.concatenate([v[:2] for v in edgs]))

        new_edgs = []
        for i in range(len(nds) - 1):
            u = nds[i]
            for j in range(i + 1, len(nds)):
                v = nds[j]
                aux = [w for w in edgs if (w[0] == u and w[1] == v) or (w[0] == v and w[1] == u)]
                a = [w[-1] for w in aux]
                new_edgs += [(u, v, aux[0][2], aux[0][3], sum(a))]

        G = nx.Graph()
        G.clear()
        for u, v, n1, n2, t in new_edgs:
            G.add_edge(u, v, nu = n1, nv = n2, k = t, w = n1  * n2 * t, w2 = 1. / n1 / n2 / t, w3 = np.pi - n1  * n2 * t)

        Cr =  list(nx.current_flow_betweenness_centrality(G, weight = "w").items())
        Cr.sort(key = lambda v: v[1], reverse = True)
        Cr = pd.DataFrame(Cr)
        Cr.columns = ["cell", "prob"]

        res = pd.DataFrame()
        res["rw_score"] = Cr["prob"]
        res.index = Cr["cell"]
        res["ncell"] = 0
        for v in res.index:
            u = [x for x in C.index if v in x]
            res["ncell"].loc[v] = C[u][0]

        unpert_rw_score = list(res.rw_score)
        #print(unpert_rw_score)  
        
        ## Subgraph Centrality (Unperturbed)
        d_nds = dict([v[::-1] for v in enumerate(G.nodes())])
        aux = [(v[0], v[1], v[2]["w"]) for v in G.edges(data = True)]

        A = np.zeros([G.order()] * 2)
        for u, v, w in aux:
            i = d_nds[u]
            j = d_nds[v]
            A[i, j] = A[j, i] = w
        D = A.sum(0)

        def f_norm(i, j):
            global D, A
            d = (D[i] * D[j]) ** (0.5)
            return 1. / d
            return 1. * A[i, j] / d

        for i in range(A.shape[0]):
            for j in range(A.shape[1]):
                A[i,j] *= f_norm(i, j)

        L, U = np.linalg.eigh(A)
        U = np.array(U)

        SC = (U ** 2) * np.exp(L)
        SC = [SC[i,:].sum() for i in range(SC.shape[0])]

        SC = pd.DataFrame(SC, index = G.nodes())
        SC.columns = ["comm"]
        SC.sort_values("comm", inplace = True, ascending = False)

        res_com = pd.DataFrame()
        res_com["SC"] = SC["comm"]
        res_com.index = SC.index
        res_com["ncell"] = 0
        for v in res_com.index:
            u = [x for x in C.index if v in x]
            res_com["ncell"].loc[v] = C[u][0]

        unpert_sc_score = list(res_com.SC)
        #print(unpert_sc_score)

        ## Random Walk (Perturbed)
        X = pd.read_excel(allcellssick_xls, sheetname = None, index_col = None)

        # Store original input sheets as lists and append unique tags
        X_upreg_gran = X['up_regulators_gran'].replace(np.nan, 'placeholder', regex = True).values.tolist()
        for i in range(0, len(X_upreg_gran)):
            X_upreg_gran[i].append(("TAG_" + id_generator(20)))
            #print(X_upreg_gran[i])
                
        X_upreg_myelo = X['up_regulators_myelo'].replace(np.nan, 'placeholder', regex = True).values.tolist()
        for i in range(0, len(X_upreg_myelo)):
            X_upreg_myelo[i].append(("TAG_" + id_generator(20)))
            #print(X_upreg_myelo[i])
                
        X_upreg_adipo = X['up_regulators_adipo'].replace(np.nan, 'placeholder', regex = True).values.tolist()
        for i in range(0, len(X_upreg_adipo)):
            X_upreg_adipo[i].append(("TAG_" + id_generator(20)))
            #print(X_upreg_adipo[i])
                
        X_upreg_macro = X['up_regulators_macro'].replace(np.nan, 'placeholder', regex = True).values.tolist()
        for i in range(0, len(X_upreg_macro)):
            X_upreg_macro[i].append(("TAG_" + id_generator(20)))
            #print(X_upreg_macro[i])
        
        X_upreg_osteo = X['up_regulators_osteo'].replace(np.nan, 'placeholder', regex = True).values.tolist()
        for i in range(0, len(X_upreg_osteo)):
            X_upreg_osteo[i].append(("TAG_" + id_generator(20)))
            #print(X_upreg_osteo[i])
        
        X_upreg_treg = X['up_regulators_treg'].replace(np.nan, 'placeholder', regex = True).values.tolist()
        for i in range(0, len(X_upreg_treg)):
            X_upreg_treg[i].append(("TAG_" + id_generator(20)))
            #print(X_upreg_treg[i])
        
        # Merge inputs and randomly remove x % of interactions
        X_upreg_perturb = X_upreg_gran + X_upreg_myelo + X_upreg_adipo + X_upreg_macro + X_upreg_osteo + X_upreg_treg
        random.shuffle(X_upreg_perturb)
        perturb_left = int(len(X_upreg_perturb)*(1-perturb_value))
        X_upreg_perturb = X_upreg_perturb[0:perturb_left]

        # Retrieve perturbed inputs back to their original input sheets
        X_pert_gran = set(map(tuple, X_upreg_gran)) & set(map(tuple, X_upreg_perturb))
        X_pert_gran = list(map(list, X_pert_gran))
        X_pert_gran = pd.DataFrame(np.array(X_pert_gran).reshape(-1,4))
        X_pert_gran.columns = ['GRAN', 'Gene', 'Score', 'Tag']
        X_pert_gran = X_pert_gran.replace('placeholder', np.nan)
        X_pert_gran = X_pert_gran.sort_values(['GRAN', 'Gene'], ascending = [True, True])
        X['up_regulators_gran'] = X_pert_gran.drop(columns = ['Tag'])
        #print(X_pert_gran)
               
        X_pert_myelo = set(map(tuple, X_upreg_myelo)) & set(map(tuple, X_upreg_perturb))
        X_pert_myelo = list(map(list, X_pert_myelo))
        X_pert_myelo = pd.DataFrame(np.array(X_pert_myelo).reshape(-1,4))
        X_pert_myelo.columns = ['MYELO', 'Gene', 'Score', 'Tag']
        X_pert_myelo = X_pert_myelo.replace('placeholder', np.nan)
        X_pert_myelo = X_pert_myelo.sort_values(['MYELO', 'Gene'], ascending = [True, True])
        X['up_regulators_myelo'] = X_pert_myelo.drop(columns = ['Tag'])
        #print(X_pert_myelo)
        
        X_pert_adipo = set(map(tuple, X_upreg_adipo)) & set(map(tuple, X_upreg_perturb))
        X_pert_adipo = list(map(list, X_pert_adipo))
        X_pert_adipo = pd.DataFrame(np.array(X_pert_adipo).reshape(-1,4))
        X_pert_adipo.columns = ['ADIPO', 'Gene', 'Score', 'Tag']
        X_pert_adipo = X_pert_adipo.replace('placeholder', np.nan)
        X_pert_adipo = X_pert_adipo.sort_values(['ADIPO', 'Gene'], ascending = [True, True])
        X['up_regulators_adipo'] = X_pert_adipo.drop(columns = ['Tag'])
        #print(X_pert_adipo)
        
        X_pert_macro = set(map(tuple, X_upreg_macro)) & set(map(tuple, X_upreg_perturb))
        X_pert_macro = list(map(list, X_pert_macro))
        X_pert_macro = pd.DataFrame(np.array(X_pert_macro).reshape(-1,4))
        X_pert_macro.columns = ['MACRO', 'Gene', 'Score', 'Tag']
        X_pert_macro = X_pert_macro.replace('placeholder', np.nan)
        X_pert_macro = X_pert_macro.sort_values(['MACRO', 'Gene'], ascending = [True, True])
        X['up_regulators_macro'] = X_pert_macro.drop(columns = ['Tag'])
        #print(X_pert_macro)
        
        X_pert_osteo = set(map(tuple, X_upreg_osteo)) & set(map(tuple, X_upreg_perturb))
        X_pert_osteo = list(map(list, X_pert_osteo))
        X_pert_osteo = pd.DataFrame(np.array(X_pert_osteo).reshape(-1,4))
        X_pert_osteo.columns = ['OSTEO', 'Gene', 'Score', 'Tag']
        X_pert_osteo = X_pert_osteo.replace('placeholder', np.nan)
        X_pert_osteo = X_pert_osteo.sort_values(['OSTEO', 'Gene'], ascending = [True, True])
        X['up_regulators_osteo'] = X_pert_osteo.drop(columns = ['Tag'])
        #print(X_pert_osteo)
        
        X_pert_treg = set(map(tuple, X_upreg_treg)) & set(map(tuple, X_upreg_perturb))
        X_pert_treg = list(map(list, X_pert_treg))
        X_pert_treg = pd.DataFrame(np.array(X_pert_treg).reshape(-1,4))
        X_pert_treg.columns = ['TREG', 'Gene', 'Score', 'Tag']
        X_pert_treg = X_pert_treg.replace('placeholder', np.nan)
        X_pert_treg = X_pert_treg.sort_values(['TREG', 'Gene'], ascending = [True, True])
        X['up_regulators_treg'] = X_pert_treg.drop(columns = ['Tag'])
        #print(X_pert_treg)
        
        #print(len(X_pert_gran) + len(X_pert_myelo) + len(X_pert_adipo) + len(X_pert_macro) + len(X_pert_osteo) + len(X_pert_treg))
               
        ks_up = [k for k in list(X.keys()) if k.startswith("up_")]
        for k in ks_up:
            z = X[k].iloc[:,0].astype("S111").isin(["nan", "", " ", "  "])
            X[k] = X[k].loc[~z,]
            X[k].columns = [X[k].columns[0].upper(), "upreg", "score"]

        ks_lig = [k for k in list(X.keys()) if k.startswith("rece")]
        for k in ks_lig:
            z = X[k].iloc[:,0].astype("S111").isin(["nan", "", " ", "  "])
            X[k] = X[k].loc[~z,]

        C = pd.read_csv(cellcountspersample_xls, sep = "\t", index_col = 0)
        C0 = C[[v for v in C.columns if "Sick" in v]]
        C = 1. * C0.sum(1) / np.sum(C0).sum()

        clls = list(X.keys())
        clls = [k for k in list(X.keys()) if k.startswith("up_")]
        clls = np.unique([v.split("_")[-1] for v in clls])
        cllx = list(X.keys())
        cllx = [k for k in list(X.keys()) if k.startswith("recep")]
        cllx = np.unique([v.split("_")[-1] for v in cllx])

        edgs = []
        upreg = {}
        for cll in clls:
            aux = X["up_regulators_%s"%cll][[cll.upper(), "upreg"]].groupby(cll.upper())["upreg"].apply(list)
            if (cll == 'treg'): aux.index = ['adipo', 'gran', 'macro', 'myelo'] 
            x = X["receptors_ligands_%s"%cll].iloc[:,0]
            w1 = [x for x in C.index if cll in x]
            for cll2 in aux.index:
                y = [x for x in aux.loc[cll2] if not isinstance(x, float)]
                w2 = [x for x in C.index if cll2 in x]
                edgs += [(cll2, cll, C[w2][0], C[w1][0], len(y))]
                if not (cll2, cll) in upreg:
                    upreg[cll2, cll] = []
                upreg[cll2, cll] = tuple(np.union1d(y, upreg[cll2, cll]))

        nds = np.unique(np.concatenate([v[:2] for v in edgs]))

        new_edgs = []
        for i in range(len(nds) - 1):
            u = nds[i]
            for j in range(i + 1, len(nds)):
                v = nds[j]
                aux = [w for w in edgs if (w[0] == u and w[1] == v) or (w[0] == v and w[1] == u)]
                a = [w[-1] for w in aux]
                new_edgs += [(u, v, aux[0][2], aux[0][3], sum(a))]

        G = nx.Graph()
        G.clear()
        for u, v, n1, n2, t in new_edgs:
            G.add_edge(u, v, nu = n1, nv = n2, k = t, w = n1  * n2 * t, w2 = 1. / n1 / n2 / t, w3 = np.pi - n1  * n2 * t)

        Cr =  list(nx.current_flow_betweenness_centrality(G, weight = "w").items())
        Cr.sort(key = lambda v: v[1], reverse = True)
        Cr = pd.DataFrame(Cr)
        Cr.columns = ["cell", "prob"]

        res = pd.DataFrame()
        res["rw_score"] = Cr["prob"]
        res.index = Cr["cell"]
        res["ncell"] = 0
        for v in res.index:
            u = [x for x in C.index if v in x]
            res["ncell"].loc[v] = C[u][0]

        pert_rw_score = list(res.rw_score)
        #print(pert_rw_score)
        
        ## Subgraph Centrality (Perturbed)
        d_nds = dict([v[::-1] for v in enumerate(G.nodes())])
        aux = [(v[0], v[1], v[2]["w"]) for v in G.edges(data = True)]

        A = np.zeros([G.order()] * 2)
        for u, v, w in aux:
            i = d_nds[u]
            j = d_nds[v]
            A[i, j] = A[j, i] = w
        D = A.sum(0)

        def f_norm(i, j):
            global D, A
            d = (D[i] * D[j]) ** (0.5)
            return 1. / d
            return 1. * A[i, j] / d

        for i in range(A.shape[0]):
            for j in range(A.shape[1]):
                A[i,j] *= f_norm(i, j)

        L, U = np.linalg.eigh(A)
        U = np.array(U)

        SC = (U ** 2) * np.exp(L)
        SC = [SC[i,:].sum() for i in range(SC.shape[0])]

        SC = pd.DataFrame(SC, index = G.nodes())
        SC.columns = ["comm"]
        SC.sort_values("comm", inplace = True, ascending = False)

        res_com = pd.DataFrame()
        res_com["SC"] = SC["comm"]
        res_com.index = SC.index
        res_com["ncell"] = 0
        for v in res_com.index:
            u = [x for x in C.index if v in x]
            res_com["ncell"].loc[v] = C[u][0]

        pert_sc_score = list(res_com.SC)
        #print(pert_sc_score)

        ## Pearson correlation coefficients for RW and SC
        RW_cor = np.corrcoef(unpert_rw_score, pert_rw_score)[1, 0]
        SC_cor = np.corrcoef(unpert_sc_score, pert_sc_score)[1, 0]

        result_rw.append(RW_cor)
        result_sc.append(SC_cor)       
              
    except (RuntimeError, TypeError, NameError, IndexError, ValueError):
    #except():
        continue
        
#print(result_rw)
#print(result_sc)   

np.savetxt("/home/dme/Documents/MCDM_project/Python_scripts/Network_analysis_results/result_rw_10%_100k_X.csv", result_rw, delimiter = "\t")
np.savetxt("/home/dme/Documents/MCDM_project/Python_scripts/Network_analysis_results/result_sc_10%_100k_X.csv", result_sc, delimiter = "\t")
print("Output calculated and stored correctly.")

