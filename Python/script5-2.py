# Author: Jordi Serra-Musach

from script0 import *
import pandas as pd
import networkx as nx
import pylab as plt

nx.graphviz_layout = nx.nx_pydot.graphviz_layout


_dir = Dir()
X = pd.read_csv(_dir.data + "cell-cell_CRC" + "mcdm_crc.csv", index_col = None, sep = "\t")

## Number of cells
C = Carrega_fitxer(_dir.data + "cell-cell_CRC" +
                  "cells_crc.txt")
C = C.strip("\r\n")
C = C.split(" ")
C = filter(lambda x: "_T_" in x, C)
C = np.array(map(lambda x: x.split("_T_")[0], C))
newC = []
for u in np.unique(C):
    v = np.sum(C == u)
    newC.append((u, v))

d_aux = {
    "B_cell" : "Bcell",
    "CD8" : "CD8",
    "Endothelial" : "endo",
    "EnterocyteLikeEpithelial" : "entero",
    "Fibroblast" : "fibro",
    "GobletLikeEpithelial" : "goblet",
    "Macrophage" : "macro",    
    "MastCell" : "Mast",
    "NK" : "NK",
    "Th17" : "th17",
    "Treg" : "Treg",
    "stemTAEpithelial" : "stemTA"
} 
C = pd.Series([v[1] for v in newC], index = [d_aux[v[0]].lower() for v in newC])
C = C / C.sum()


## upstream regulators
######################


edgs = []
upreg = {}
for i in range(X.shape[0]):
    cll0, cll1, y = X.iloc[i]
    cll0 = cll0.lower()
    cll1 = cll1.lower()
    if not (cll0, cll1) in upreg:
        upreg[cll0, cll1] = []
    upreg[cll0, cll1] = tuple(np.union1d([y], upreg[cll0, cll1]))
    edgs += [(cll0, cll1, C[cll0], C[cll1], 1)]

edgs = {}
for k in upreg:
    k1 = list(k)
    k1.sort()
    k1 = tuple(k1)
    if not k1 in edgs:
        edgs[k1] = []
    edgs[k1] += upreg[k]


nds = np.unique(np.concatenate([v for v in edgs]))

new_edgs = []
for u, v in edgs:
    t = len(edgs[u,v])
    new_edgs += [(u, v, C[u], C[v], t)]

# for i in range(len(nds) - 1):
#     u = nds[i]
#     for j in range(i + 1, len(nds)):
#         v = nds[j]
#         if not ((u, v) in G.edges() or (v, u) in G.edges()):
#             new_edgs += [(u, v, C[u], C[v], 0)]

 
       
G = nx.Graph()
G.clear()
for u, v, n1, n2, t in new_edgs:
    G.add_edge(u, v, nu = n1, nv = n2, k = t, w = n1  * n2 * t, w2 = 1. / n1 / n2 / t, w3 = np.pi - n1  * n2 * t)


## Random walk score
Cr =  nx.current_flow_betweenness_centrality(G, weight = "w").items()
Cr.sort(key = lambda v: v[1], reverse = True)
Cr = pd.DataFrame(Cr)
Cr.columns = ["cell", "prob"]

ns = Cr["prob"]
mn, mx = ns.min(), ns.max()
ns = ns - mn
ns = ns / (mx - mn)
ns = ns * (2000 - 200) + 200
ns = dict([(Cr["cell"][i], ns[i]) for i in range(len(ns))])
ns = [ns[k] for k in G.nodes()]
pos = nx.graphviz_layout(G)

plt.figure()
nx.draw(G, labels = dict(zip(*[G.nodes()] * 2)), pos = pos, node_size = ns, node_color = "#ffaa00",
        edgecolors = "#ee8855", linewidths = [max(v * .0025, 3) for v in ns])

plt.savefig(_dir.images + "cell-cell_crc-network.pdf")


res = pd.DataFrame()
res["rw_score"] = Cr["prob"]
res.index = Cr["cell"]
res["ncell"] = 0
for v in res.index:
    u = filter(lambda x: v in x, C.index)
    res["ncell"].loc[v] = C[u][0]

res.to_csv(_dir.images + "cell-cell_crc-network_centrality.tsv", sep = "\t")
